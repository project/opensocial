<?php

// Bootstrap Drupal
chdir($_SERVER['DOCUMENT_ROOT'] );
include_once("./bootstrap.inc"); //PATH NEEDS TO BE FETCHED  


class PersonClassService implements PersonService{

  public function getPerson($userId, $groupId, $fields, SecurityToken $token) {
    if (! is_object($groupId)) {
      throw new SocialSpiException("Not Implemented", ResponseError::$NOT_IMPLEMENTED);
    }
    $person = $this->getPeople($userId,$groupId ,new CollectionOptions(),$fields, $token);
    // return of getPeople is a ResponseItem(RestfulCollection(ArrayOfPeople)), disassemble to return just one person
    $person = $person->getResponse()->getEntry();
    if (is_array($person) && count($person) == 1) {
      return new ResponseItem(null, null, $person[0]);
    }
    return new ResponseItem(NOT_FOUND, "Person not found", null);
  }

  
  
  public function getPeople($userId, $groupId, CollectionOptions $options , $fields, SecurityToken $token) {
  
    $ortOrder = $options->getSortOrder();
    $filter = $options->getFilterBy();
    $first = $options->getStartIndex();
    $max = $options->getCount();
    $networkDistance = $options->getNetworkDistance();
    $ids = $this->getIdSet($userId, $groupId, $token);
    
    //$uid = implode(",",$ids);
    //echo $list;
    // This is the place where i get in all the details of the users whose uids have been already passed in the $ids
    foreach($result as $ids){
      $users[] = user_load( array('uid' => $uid));
      $person_id =        // userId
      $name = new Name(   ); //username
      
      //Create an instance of Person class
      //set id as  first argument, and name object as second

      $person = new Person($person_id, $name);
      $person->setDateOfBirth();  //depending upon the data fetched all this has to be initialized
      $person->setAboutMe();
      $person->setGender()
      $list[$person_id]=$person;    
      // Needs to be worked upon ...........
    }
          
    if ($sortOrder == 'name') {
      usort($people, array($this, 'comparator'));
    }
    //TODO: The samplecontainer doesn't support any filters yet. We should fix this.
    $totalSize = count($people);
    $last = $first + $max;
    $last = min($last, $totalSize);
    if ($first !== false && $first != null && $last) {
      $people = array_slice($people, $first, $last);
    }
    $collection = new RestfulCollection($people, $first, $totalSize);
    return new ResponseItem(null, null, $collection);
  }


  /**
   * Determines whether the input is a valid key.
   *
   * @param key the key to validate.
   * @return true if the key is a valid appdata key, false otherwise.
   * 
   * NOTE FROM TONY: THIS COULD BE DONE IN 1 LINE USING PREG_MATCH
   */
  public static function isValidKey($key) {
    if (empty($key)) {
      return false;
    }
    for ($i = 0; $i < strlen($key); ++ $i) {
      $c = substr($key, $i, 1);
      if (($c >= 'a' && $c <= 'z') || ($c >= 'A' && $c <= 'Z') || ($c >= '0' && $c <= '9') || ($c == '-') || ($c == '_') || ($c == '.')) {
        continue;
      }
      return false;
    }
    return true;
  }

  private function comparator($person, $person1)
  {
    $name = $person['name']['unstructured'];
    $name1 = $person1['name']['unstructured'];
    if ($name == $name1) {
      return 0;
    }
    return ($name < $name1) ? - 1 : 1;
  }

  private function getIdSet($user, GroupId $group, SecurityToken $token) {
    $ids = array();
    if ($user instanceof UserId) {
      $userId = $user->getUserId($token);
      if ($group == null) {
        return array($userId);
      }
      switch ($group->getType()) {
        case 'self':
          $ids[] = $userId;
          break;
        case 'all':
        case 'friends':
        // Tony i need was trying to run the socknet here but seems some issse. What i need here is just the ids of the friend of the user whose is already fetched in $userId in few lines above. As if now assuming tha the logged in user had just 1 freind whose id is 4........ you need to fix this 
          $ids[]= 3;
            //$result = (array) module_invoke_all('socnet_get_related_users', $uid,NULL,'two-way');
            //$result = array_unique($result);
        break;
        default:
          $ids[] = $userId->getUserId($token);
          return new ResponseItem(NOT_IMPLEMENTED, "We don't support fetching data in batches yet", null);
          break;
      }
    } elseif (is_array($user)) {
      $ids = array();
      foreach ($user as $id) {
        $ids = array_merge($ids, $this->getIdSet($id, $group, $token));
      }
    }
    return $ids;
  }



}

