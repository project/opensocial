<?php

/*
This is a basic class to display gadgets using the meta data server. Also this is not a comlete class, 
It is right now used for developement process. We would be enhancing its preferences as we come up 
with other API's,
Note: This is just a crude representation to get gadgets working */


class RenderGadget {
  
  function RenderGadget(){
  }
    
  function fetch_gadget_metadata($view,$moduleId,$app_url){

    $request = json_encode(array(
      'context' => array(
         'view' =>$view, 
         'debug' =>true,
         'container' => 'default',
         'ignoreCache'=>true,
      ), 
      'gadgets' => array(
        array('url' => $app_url, 'moduleId' => $moduleId),
      ),
      'prefs' => array('bkgcolor'),
    ));
             
    $ch = curl_init();
    $meta_data_server="http://".$_SERVER['HTTP_HOST']."/shindig/php/gadgets/metadata";
    curl_setopt($ch, CURLOPT_URL, $meta_data_server);
    curl_setopt($ch, CURLOPT_TRANSFERTEXT, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 4);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, 'request=' . urlencode($request));
    $content = @curl_exec($ch);
    return json_decode($content);
  }
  
  function getExtraURLParams( $ownerId, $app_url){
    $viewerId =  $ownerId; 

    $extraParams ='&owner=' . $ownerId . 
                  '&viewer=' . $viewerId . 
                  '&st=' . getSecureToken($ownerId,$viewerId,$app_url);
    return $extraParams;
  }

}
  
function  getSecureToken($ownerId,$viewerId,$app_url){

  // TODO: Remove hardcodings - AppId=1, Container=shindig, ModuleId=1
  $encodedGadgetURL = urlencode($app_url);
  $token =  $ownerId . 
            ':' .
            $viewerId .
            ':' .
            '1' .
            ':shindig:' .
            $encodedGadgetURL .
            ':'.
            '1';
  $encodedToken = urlencode($token);
  return $encodedToken;
}    
  
function setInfo($gadget){
  $info['url'] =$gadget->url;
  $info['title'] = isset($gadget->title) ? $gadget->title : '';
  $info['directory_title'] = isset($gadget->directoryTitle) ? $gadget->directoryTitle : '';
  $info['height'] = isset($gadget->height) ? $gadget->height : '';
  $info['screenshot'] = isset($gadget->screenshot) ? $gadget->screenshot : '';
  $info['thumbnail'] = isset($gadget->thumbnail) ? $gadget->thumbnail : '';
  $info['author'] = isset($gadget->author) ? $gadget->author : '';
  $info['author_email'] = isset($gadget->authorEmail) ? $gadget->authorEmail : '';
  $info['description'] = isset($gadget->description) ? $gadget->description : '';
  $info['settings'] = isset($gadget->userPrefs) ? serialize($gadget->userPrefs) : '';
  $info['scrolling'] = ! empty($gadget->scrolling) ? $gadget->scrolling : '0';
  $info['height'] = ! empty($gadget->height) ? $gadget->height : '0';
  // extract the version from the iframe url
  $iframe_url = $gadget->iframeUrl;
  return $iframe_url;
}
  
